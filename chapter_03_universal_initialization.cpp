//
// Created by crypt on 15/01/19.
//

class foo
{
  int a_;
 public:
  foo(int a):a_(a) {}
};

int main() {
  // foo f1;           // default initialization
  foo f2( 1.2);
  foo f3(42);
  foo f4();         // function declaration
  foo cc = {1.2};

}
